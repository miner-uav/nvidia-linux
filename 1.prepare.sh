#!/usr/bin/env bash

#
# Setup Environment
#

. $(dirname $0)/environment.sh


#
# Clean the environment
#
echo "cleaning the build and package dirs"
rm -rf $AUV_KERNEL_OUT
rm -rf $AUV_KERNEL_PACKAGE
mkdir $AUV_KERNEL_OUT
mkdir $AUV_KERNEL_PACKAGE

#echo "cleaning up git sources"
#git checkout auvidea_j120 --force
#git checkout - .

pushd $AUV_KERNEL_SRC


make mrproper 
make O=$AUV_KERNEL_OUT mrproper
popd
