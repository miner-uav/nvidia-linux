#!/usr/bin/env bash

#
# Setup Environment
#

. $(dirname $0)/environment.sh

pushd $AUV_KERNEL_SRC

#make the config base
echo "Building default config"
make O=$AUV_KERNEL_OUT tegra18_defconfig

make O=$AUV_KERNEL_OUT menuconfig

make O=$AUV_KERNEL_OUT kernelversion

#return to origin
popd
