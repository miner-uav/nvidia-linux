#!/usr/bin/env bash

#
# Setup Environment
#

. $(dirname $0)/environment.sh

pushd $AUV_KERNEL_SRC

AUV_KERNEL_VERSION=`make O=$AUV_KERNEL_OUT kernelversion`
echo "Build name: $AUV_KERNEL_VERSION"

echo "Building zImage"
make O=$AUV_KERNEL_OUT zImage -j6

echo "Building DTBs"
make O=$AUV_KERNEL_OUT dtbs 

echo "Building modules"
make O=$AUV_KERNEL_OUT modules -j6

#return to origin
popd
