#!/usr/bin/env bash

#
# Setup Environment
#

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

. $(dirname $0)/environment.sh

if [ ! -d $AUV_KERNEL_PACKAGE/boot ]; then
	mkdir -p $AUV_KERNEL_PACKAGE/boot
	chown -R $AUV_USER:$AUV_USER $AUV_KERNEL_PACKAGE
fi

echo "Cleaning up the initrd src folder.."
rm -rf $AUV_KERNEL_INITRD
mkdir $AUV_KERNEL_INITRD

pushd $AUV_KERNEL_INITRD


echo "Unpacking initrd: $JETPACK/64_TX1/Linux_for_Tegra_64_tx1/rootfs/boot/initrd"
gzip -cd $JETPACK/64_TX1/Linux_for_Tegra_64_tx2/rootfs/boot/initrd | cpio -imd --quiet


patch $AUV_KERNEL_INITRD/init $AUV_KERNEL_SRC/auvidea/initrd_init.patch

find . | cpio --quiet -H newc -o | gzip -9 -n > $AUV_KERNEL_PACKAGE/boot/initrd.auv

chown  $AUV_USER:$AUV_USER $AUV_KERNEL_PACKAGE/boot/initrd.auv
#return to origin
popd
