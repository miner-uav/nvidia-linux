#!/usr/bin/env bash

#
# Setup Environment
#

. $(dirname $0)/environment.sh

pushd $AUV_KERNEL_SRC

export AUV_KERNEL_REL=`make O=$AUV_KERNEL_OUT kernelrelease`

echo "Installing modules to:$AUV_KERNEL_PACKAGE"
make O=$AUV_KERNEL_OUT modules_install INSTALL_MOD_PATH=$AUV_KERNEL_PACKAGE


#clean links to build and sources
#rm $AUV_KERNEL_PACKAGE/lib/modules/$AUV_KERNEL_REL/build
#rm $AUV_KERNEL_PACKAGE/lib/modules/$AUV_KERNEL_REL/source


if [ ! -d $AUV_KERNEL_PACKAGE/boot ]; then
	mkdir -p $AUV_KERNEL_PACKAGE/boot
fi

cp -v $AUV_KERNEL_OUT/arch/arm64/boot/Image $JETPACK/64_TX2/Linux_for_Tegra_tx2/kernel/Image.auv
cp -v $AUV_KERNEL_OUT/arch/arm64/boot/zImage $JETPACK/64_TX2/Linux_for_Tegra_tx2/kernel/zImage.auv
cp -v $AUV_KERNEL_OUT/arch/arm64/boot/dts/tegra186* $JETPACK/64_TX2/Linux_for_Tegra_tx2/kernel/dtb/
#modified initrd to allow nvme boot


pushd $AUV_KERNEL_PACKAGE
tar --owner root --group root -cjf kernel_supplements.tbz2 lib/modules
popd


cp -v $AUV_KERNEL_PACKAGE/kernel_supplements.tbz2 $JETPACK/64_TX2/Linux_for_Tegra_tx2/kernel/


#return to origin
popd
