copy tegra186-quill-power-tree-p3310-1000-a00-00.dtsi -> to kernel-src/hardware/nvidia/platform/t18x/common/kernel-dts/t18x-common-platforms/

rebuild kernel btw. rebuild dtb files


copy tegra186-mb1-bct-pmic-quill-p3310-1000-c03.cfg -> to 64_TX2/Linux_for_tx2/bootloader/t186ref/BCT
copy tegra186-mb1-bct-pmic-quill-p3310-1000-c04.cfg -> to 64_TX2/Linux_for_tx2/bootloader/t186ref/BCT

reflash Jetson TX2


summary of changes:
tegra186-quill-power-tree-p3310-1000-a00-00.dtsi:113

	pinctrl@3520000 {
		vbus-0-supply = <&vdd_usb0_5v>;
		vbus-1-supply = <&vdd_usb1_5v>;
		//vbus-2-supply = <&vdd_usb2_5v>;
		vbus-2-supply = <&battery_reg>;
		//vbus-3-supply = <&battery_reg>;
		vbus-3-supply = <&vdd_usb2_5v>;

rest in screenshoots
