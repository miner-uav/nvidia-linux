#!/usr/bin/env bash

#
# Setup Environment
#

export JETPACK=/data/JetPack3.1
export AUV_BUILD_NAME=J120_2.2.0

export AUV_BASE_DIR=/data/git/nvidia-linux
export AUV_KERNEL_SRC=$AUV_BASE_DIR/sources/kernel/kernel-4.4/
export AUV_LINARO_DIR=/data/tools/linaro
export AUV_USER=maciej

# tools for r24.2.1
export CROSS_COMPILE=$AUV_LINARO_DIR/gcc-linaro-5.3-2016.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
export CROSS32CC=$AUV_LINARO_DIR/gcc-linaro-5.3-2016.02-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc


export AUV_KERNEL_INITRD=$AUV_BASE_DIR/initrd
export AUV_KERNEL_OUT=$AUV_KERNEL_SRC../build_out
export AUV_KERNEL_PACKAGE=$AUV_KERNEL_SRC../build_package
export ARCH=arm64

echo "# ENVIRONMENT"

echo "LINARO DIR:         $AUV_LINARO_DIR"
echo "JETPACK:            $JETPACK"
echo "KERNEL SOURCE:      $AUV_KERNEL_SRC"
echo "OUTPUT DIR:         $AUV_KERNEL_OUT"
echo "PACKAGE_DIR:        $AUV_KERNEL_PACKAGE"
echo "INITRD:             $AUV_KERNEL_INITRD"



