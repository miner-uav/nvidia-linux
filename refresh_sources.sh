#!/usr/bin/env bash

. $(dirname $0)/environment.sh 


rm -rf sources
if [ ! -f temp/source_release.tbz2 ]; then
    mkdir -p temp
	pushd temp
	wget http://developer.download.nvidia.com/embedded/L4T/r28_Release_v1.0/BSP/source_release.tbz2
	popd
fi
if [ ! -f temp/sources/kernel_src-tx2.tbz2 ]; then
    pushd temp
	tar -xvf source_release.tbz2 sources/kernel_src-tx2.tbz2
	popd
fi


mkdir -p sources
pushd sources
tar -xvf ../temp/sources/kernel_src-tx2.tbz2
popd


# patch for the auvidea USB3 
cp -v ChangesTX2J140/tegra186-quill-power-tree-p3310-1000-a00-00.dtsi \
sources/hardware/nvidia/platform/t18x/common/kernel-dts/t18x-common-platforms/tegra186-quill-power-tree-p3310-1000-a00-00.dtsi

cp -v ChangesTX2J140/tegra186-mb1-bct-pmic-quill-p3310-1000-c03.cfg \
$JETPACK/64_TX2/Linux_for_Tegra_tx2/bootloader/t186ref/BCT/tegra186-mb1-bct-pmic-quill-p3310-1000-c03.cfg

cp -v ChangesTX2J140/tegra186-mb1-bct-pmic-quill-p3310-1000-c04.cfg \
$JETPACK/64_TX2/Linux_for_Tegra_tx2/bootloader/t186ref/BCT/tegra186-mb1-bct-pmic-quill-p3310-1000-c04.cfg

